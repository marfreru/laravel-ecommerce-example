FROM php:7.2-fpm

RUN apt-get update && apt-get install -y libmcrypt-dev libpng-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libicu-dev \
    libpq-dev \
    libxpm-dev \
    libvpx-dev \
    zlib1g-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo zip pgsql pdo_pgsql